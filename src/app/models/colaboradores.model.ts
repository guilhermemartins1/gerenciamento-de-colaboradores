export type Colaboradores = {
  first: number,
  prev: number | null,
  next: number | null,
  last: number,
  pages: number,
  items: number,
  data: Array<any>
}
export type Colaborador = {
  id: string | undefined,
  nome: string,
  cpf: string,
  dataAdmissao: string,
  remuneracao: string,
  cargo: CargoID
}

export type Cargos = {
  id: number,
  descricao: string
}
export type CargoID = {
  id: number,
}