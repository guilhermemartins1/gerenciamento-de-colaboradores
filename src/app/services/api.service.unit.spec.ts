import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { Colaboradores, Colaborador, Cargos } from '../models/colaboradores.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

const mockEnvironment = { api: 'http://localhost:3000/' };

beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      ApiService,
      { provide: 'environment', useValue: mockEnvironment }
    ]
  });
});

it('get colaboradores', (done) => {
  const expectedColaboradores: Colaboradores = {
    first: 1,
    prev: null,
    next: null,
    last: 1,
    pages: 1,
    items: 2,
    data: [
        {
            "id": "fae1",
            "nome": "Pedro Mauricio",
            "cpf": "123.456.789-09",
            "dataAdmissao": "20/02/2024",
            "remuneracao": 2000,
            "cargo": {
              "id": 2
            }
          },
          {
            "id": "fae2",
            "nome": "Maria Silva",
            "cpf": "987.654.321-10",
            "dataAdmissao": "15/03/2024",
            "remuneracao": 3500,
            "cargo": {
              "id": 2
            }
          },
    ]
  };

  const mockHttpClient = TestBed.inject(HttpClient);
  spyOn(mockHttpClient, 'get').and.returnValue(of(expectedColaboradores));

  const apiService = TestBed.inject(ApiService);
  apiService.getColaboradores(1).subscribe(
    (data) => {
      expect(data).toEqual(expectedColaboradores);
      done();
    },
    (error) => fail(error)
  );
});

it('get colaboradores page', (done) => {
  const mockHttpClient = TestBed.inject(HttpClient);
  const mockError = new HttpErrorResponse({ error: 'Something went wrong' });
  spyOn(mockHttpClient, 'get').and.returnValue(throwError(mockError));

  const apiService = TestBed.inject(ApiService);
  apiService.getColaboradores(1).subscribe(
    (data) => fail('Unexpected success'),
    (error) => {
      expect(error).toEqual(mockError);
      done();
    }
  );
});

it('post colaborador', (done) => {
  const newColaborador: Colaborador = 
  {
    id: "test1",
    nome: "Pedro Pedro",
    cpf: "123.456.789-09",
    dataAdmissao: "20/02/2024",
    remuneracao: "2000",
    cargo: {
        id: 2
    }
  };
  const expectedResponse: Colaborador = {
    id: "test1",
    nome: "Pedro Pedro",
    cpf: "123.456.789-09",
    dataAdmissao: "20/02/2024",
    remuneracao: "2000",
    cargo: {
        id: 2
    }
  }; // Assign generated ID

  const mockHttpClient = TestBed.inject(HttpClient);
  spyOn(mockHttpClient, 'post').and.returnValue(of(expectedResponse));

  const apiService = TestBed.inject(ApiService);
  apiService.postColaborador(newColaborador).subscribe(
    (data) => {
      expect(data).toEqual(expectedResponse);
      done();
    },
    (error) => fail(error)
  );
});


