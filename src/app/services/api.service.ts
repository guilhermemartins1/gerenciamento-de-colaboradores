import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { environment } from "src/environments/environment";
import { Cargos, Colaborador, Colaboradores } from "../models/colaboradores.model";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private url = `${environment.api}`;

  constructor(private httpClient: HttpClient){
  }
  getColaboradores(page: number){
    var uri = `${this.url}/colaboradores?_page=${page}`;
    return this.httpClient.get<Colaboradores>(uri);
  }
  getCargos(){
    var uri = `${this.url}/cargos`;
    return this.httpClient.get<Cargos[]>(uri);
  }
  postColaborador(colaborador: Colaborador){
    var uri = `${this.url}/colaboradores`;
    return this.httpClient.post<Colaborador>(uri, colaborador);
  }
  putColaborador(id: number, colaborador: Colaborador){
    var uri = `${this.url}/colaboradores/${id}`;
    return this.httpClient.put<Colaborador>(uri, colaborador);
  }
  delete(id: number){
    var uri = `${this.url}/colaboradores/${id}`;
    return this.httpClient.delete<void>(uri);
  }
}