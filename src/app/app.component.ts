import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiService } from './services/api.service';
import { Cargos, CargoID, Colaborador, Colaboradores } from './models/colaboradores.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  colaborador!: Colaborador;
  colaboradores$ : Colaboradores | undefined;
  cargos$ : Cargos[]| undefined;

  page$ = 1;
  pages$ = 2;
  add$ = false;
  create$ = false;
  edit$ = false;

  id : any;
  nome: any;
  cpf: any;
  dataAdmissao: any;
  remuneracao: any;
  cargo: any;

  constructor(private apiService: ApiService){
    this.getCargos();
    this.getColaboradores(this.page$);
  }
  getCargos(page:number = this.page$){
    if(page == null){return;}
    var data = this.apiService.getCargos();
    data.forEach(value => {
      this.cargos$ = value;
    })
  }  
  getColaboradores(page:number = this.page$){
    if(page == null){return;}
    this.page$ = page;

    var data = this.apiService.getColaboradores(page);
    data.forEach(value => {
      this.colaboradores$ = value;
      //console.log(value);
      if(this.colaboradores$?.next == null)
        {
          this.page$ = this.colaboradores$?.last!;
        }
        if(this.colaboradores$?.prev == null)
          {
            this.page$ = this.colaboradores$?.first!;
          }
    });     
  }
  create()
  {
    if(this.validateForm() && this.validateCPF())
      {
        this.close();
        var cargo : CargoID;
        cargo = {id:this.cargo}
        var c : Colaborador;
        console.log(this.dataAdmissao);

        const s = this.dataAdmissao.split('-');
        var date = new Date();
        date.setFullYear(+s[0], +s[1]-1, +s[2]);

        c = {
          id: undefined,
          nome: this.nome,
          cpf: this.cpf,
          dataAdmissao: this.dateDate(date),
          remuneracao: this.remuneracao,
          cargo : cargo
        }
        console.log(c);
        var post = this.apiService.postColaborador(c)
        .subscribe(_ => this.getColaboradores());
        // post.forEach(value =>{
        //   console.log(value);
        // })
      }
      else
      {

      }
  }
  add(){
    this.add$ = true;
    //this.colaborador = undefined;
    //console.log(this.colaborador);
    this.id = undefined;
    this.nome= undefined;
    this.cpf= undefined;
    this.dataAdmissao = undefined;
    this.remuneracao= undefined;
    this.cargo = undefined;
  }
  close()
  {
    this.add$ = false;
    this.edit$ = false;
  }
  edit(colaborador: Colaborador){
    this.edit$ = true;
    this.colaborador = colaborador;
    console.log(this.colaborador);
    this.id = colaborador.id;
    this.nome= colaborador.nome;
    this.cpf= colaborador.cpf;
    const s = colaborador.dataAdmissao.split('/');
    var date = new Date();
    date.setFullYear(+s[2], +s[1]-1, +s[0]);
    this.dataAdmissao = this.dateYear(date);
    this.remuneracao= colaborador.remuneracao;
    this.cargo = colaborador.cargo.id;
  }
  dateYear(date : Date)
  {
    return `${date.getFullYear().toString().padStart(4,'0000')}-${(date.getMonth()+1).toString().padStart(2,'00')}-${date.getDate().toString().padStart(2,'00')}`;
  }
  dateDate(date : Date)
  {
    return `${date.getDate().toString().padStart(2,'00')}/${(date.getMonth()+1).toString().padStart(2,'00')}/${date.getFullYear().toString().padStart(4,'0000')}`;
  }
  update(){
    if( this.validateForm() && this.validateCPF())
      {
        this.close();
        var cargo : CargoID;
        cargo = {id:this.cargo}
        var c : Colaborador;
        var s = this.dataAdmissao.split('-');
        var date = new Date();
        date.setFullYear(+s[0], +s[1]-1, +s[2]);
        this.dataAdmissao = this.dateDate(date);
        c = {
          id: this.id,
          nome: this.nome,
          cpf: this.cpf,
          dataAdmissao: this.dataAdmissao,
          remuneracao: this.remuneracao,
          cargo : cargo
        }
        console.log(c);
        var post = this.apiService.putColaborador(this.id, c)
        .subscribe(_ => this.getColaboradores());
        // post.forEach(value =>{
        //   console.log(value);
        // })
      }
      else
      {

      }
  }
  remove(id: number){
    this.apiService.delete(id)
      .subscribe(_ => this.getColaboradores());
  }

  validateForm(): boolean {
    if (!this.nome || !this.cpf || !this.dataAdmissao || !this.remuneracao || !this.cargo) {
      return false;
    }
    return true;
  }
   validateCPF(): boolean {
    const cpfDigits = this.cpf.replace(/[^0-9]/g, "");
    if (cpfDigits.length !== 11) {
      return false;
    }
    let firstDigitCheckSum = 0;
    for (let i = 0; i < 9; i++) {
      firstDigitCheckSum += parseInt(cpfDigits[i]) * (10 - i);
    }
    let secondDigitCheckSum = firstDigitCheckSum % 11;
    if (secondDigitCheckSum === 10) {
      secondDigitCheckSum = 0;
    }
    const calculatedDigits = `${cpfDigits[9]}${cpfDigits[10]}`;
    const providedDigits = cpfDigits.slice(-2);
    if (calculatedDigits !== providedDigits) {
      return false;
    }
    return true;
  }
}
